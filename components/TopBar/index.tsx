import { AppBar, Container, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import styles from "./index.module.css";

const TopBar: React.FC = () => {
    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        className={styles.title}
                        variant="h6"
                        component="div"
                    >
                        Torrent Streamer
                    </Typography>
                    <Typography variant="subtitle1">💚 illemonati</Typography>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default TopBar;
