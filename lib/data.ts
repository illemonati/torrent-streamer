export interface TorrentFiles {
    [key: string]: TorrentFile;
}

export interface TorrentFile {
    name: string;
    path: string;
    length: number;
}
export interface ErrorResult {
    error: string;
}
