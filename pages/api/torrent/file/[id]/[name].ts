import { NextApiRequest, NextApiResponse } from "next";
import torrentStream from "torrent-stream";
import mime from "mime-types";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const { id, name } = req.query;
        const { torrentURI, filePath } = (global as any).torrentStreamUrls[
            id as string
        ];
        console.log(id, name);
        const engine = torrentStream(torrentURI, {
            uploads: 0,
            tmp: process.env.TSTREAM_TMP,
        });

        await new Promise((r) => engine.on("ready", r));
        let fileToUse = null;
        for (const file of engine.files) {
            if (file.path === filePath) {
                fileToUse = file;
            }
        }
        if (!fileToUse) {
            throw new Error("File not found in torrent");
        }
        res.writeHead(200, {
            "Content-Type":
                mime.contentType(fileToUse.name) || "application/octet-stream",
            "Content-Length": fileToUse.length,
        });
        const stream = fileToUse.createReadStream();
        stream.pipe(res);
    } catch (err: any) {
        res.status(500).json({
            error: err.message,
        });
    }
};

export default handler;
