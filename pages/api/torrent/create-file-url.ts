import { NextApiRequest, NextApiResponse } from "next";
import { ErrorResult } from "lib/data";
import { createNewTorrentStreamURLAndAddToPool } from "lib/torrent-streams";

interface FileUrlResult {
    url: string;
}

const handler = async (
    req: NextApiRequest,
    res: NextApiResponse<ErrorResult | FileUrlResult>
) => {
    try {
        const { torrentURI, filePath, fileName } = req.body;
        const streamURL = createNewTorrentStreamURLAndAddToPool(
            torrentURI,
            filePath
        );

        res.status(200).json({
            url: `/api/torrent/file/${streamURL.id}/${fileName}`,
        });
    } catch (err: any) {
        res.status(500).json({
            error: err.message,
        });
    }
};

export default handler;
