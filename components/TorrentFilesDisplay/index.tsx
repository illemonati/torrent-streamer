import { Fab } from "@material-ui/core";
import {
    DataGrid,
    GridColDef,
    GridRenderCellParams,
    GridRowsProp,
    GridValueFormatterParams,
} from "@mui/x-data-grid";
import fileSize from "filesize";
import { TorrentFiles } from "lib/data";
import React, { useState } from "react";
import { useEffect } from "react";
import styles from "./index.module.css";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import axios from "axios";

interface TorrentFilesDisplayProps {
    torrentFiles: TorrentFiles;
    torrentURI: string;
    showURL: (url: string) => any;
}

const TorrentFilesDisplay: React.FC<TorrentFilesDisplayProps> = (props) => {
    const { torrentFiles, torrentURI, showURL } = props;
    const [rows, setRows] = useState<GridRowsProp>([]);

    const onGetLinkButtonClick = async (filePath: string) => {
        const resp = await axios.post("/api/torrent/create-file-url", {
            torrentURI,
            filePath,
            fileName: torrentFiles[filePath].name,
        });
        const { url } = resp.data;
        showURL(url);
    };

    useEffect(() => {
        setRows(() =>
            Object.entries(torrentFiles).map(([id, file]) => {
                return {
                    id,
                    name: file.name,
                    size: file.length,
                    link: file.path,
                };
            })
        );
    }, [props]);

    const columns: GridColDef[] = [
        {
            field: "size",
            headerName: "Size",
            width: 100,
            valueFormatter: (params: GridValueFormatterParams) => {
                return fileSize(params.value as number);
            },
        },
        { field: "name", headerName: "Name", minWidth: 250, flex: 1 },
        {
            field: "link",
            headerName: "Link",
            width: 100,
            renderCell: (params: GridRenderCellParams) => {
                return (
                    <Fab
                        color="secondary"
                        size="small"
                        onClick={() =>
                            onGetLinkButtonClick(params.value as string)
                        }
                    >
                        <FileDownloadIcon />
                    </Fab>
                );
            },
        },
    ];

    return (
        <DataGrid
            className={styles.DataGridContainer}
            rows={rows}
            columns={columns}
            initialState={{
                sorting: {
                    sortModel: [{ field: "name", sort: "asc" }],
                },
            }}
        />
    );
};

export default TorrentFilesDisplay;
