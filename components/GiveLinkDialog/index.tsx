import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button,
    Grid,
    TextField,
    IconButton,
    InputAdornment,
    OutlinedInput,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import CheckIcon from "@mui/icons-material/Check";

interface GiveLinkDialogProps {
    url: string;
    open: boolean;
    handleClose: () => any;
}

const GiveLinkDialog: React.FC<GiveLinkDialogProps> = (props) => {
    const { url, open, handleClose: parentHandleClose } = props;
    const [modifiedURL, setModifiedURL] = useState("");
    const [copied, setCopied] = useState(false);

    useEffect(() => {
        setModifiedURL(() => new URL(url, document.baseURI).href);
    }, [props]);

    const handleCopyClick = () => {
        setCopied(true);
        navigator.clipboard.writeText(modifiedURL);
    };

    const handleClose = () => {
        setCopied(false);
        parentHandleClose();
    };

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle id="alert-dialog-title">Heres your link</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Copy it and use it with your preferred downloading or
                    streaming client.
                </DialogContentText>

                <OutlinedInput
                    fullWidth
                    value={modifiedURL}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton edge="end" onClick={handleCopyClick}>
                                {copied ? <CheckIcon /> : <ContentCopyIcon />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
};

export default GiveLinkDialog;
