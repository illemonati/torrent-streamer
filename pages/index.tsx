import {
    Container,
    Fab,
    Grid,
    Paper,
    TextField,
    Typography,
} from "@material-ui/core";
import type { NextPage } from "next";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import styles from "../styles/Home.module.css";
import { ChangeEvent, useState } from "react";
import axios from "axios";
import { TorrentFiles } from "lib/data";
import TorrentFilesDisplay from "components/TorrentFilesDisplay";
import GiveLinkDialog from "components/GiveLinkDialog";

const Home: NextPage = () => {
    const [torrentURI, setTorrentURI] = useState("");
    const [giveLinkURL, setGiveLinkURL] = useState("");
    const [torrentFiles, setTorrentFiles] = useState<TorrentFiles | null>(null);

    const onTorrentInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setTorrentURI(e.target.value);
    };

    const onFileListButtonClick = async () => {
        try {
            const resp = await axios.post("/api/torrent/get-files", {
                torrentURI,
            });
            setTorrentFiles(resp.data);
        } catch (err) {
            console.error(err);
        }
    };

    const showURL = (url: string) => {
        setGiveLinkURL(() => url);
    };

    const handleGiveLinkClose = () => {
        setGiveLinkURL(() => "");
    };

    return (
        <div className={styles.HomePage}>
            <Container maxWidth="md" className={styles.MainContainer}>
                <Grid
                    container
                    direction="column"
                    className={styles.MainGrid}
                    spacing={2}
                >
                    <Grid item className={styles.TopContainer}>
                        <Paper variant="outlined">
                            <Container
                                className={styles.InputContainer}
                                maxWidth="md"
                            >
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Typography variant="h6">
                                            Stream torrent from ...
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={9}>
                                        <TextField
                                            onChange={onTorrentInputChange}
                                            variant="outlined"
                                            fullWidth
                                            label="Torrent URI"
                                        />
                                    </Grid>
                                    <Grid
                                        className={styles.SearchButtonContainer}
                                        item
                                        xs={3}
                                    >
                                        <Fab
                                            color="secondary"
                                            variant="extended"
                                            onClick={onFileListButtonClick}
                                        >
                                            <ManageSearchIcon />
                                            Files
                                        </Fab>
                                    </Grid>
                                </Grid>
                            </Container>
                        </Paper>
                    </Grid>
                    <Grid item className={styles.BottomContainer}>
                        {torrentFiles && (
                            <TorrentFilesDisplay
                                torrentFiles={torrentFiles}
                                torrentURI={torrentURI}
                                showURL={showURL}
                            />
                        )}
                    </Grid>
                </Grid>
            </Container>
            <GiveLinkDialog
                url={giveLinkURL}
                open={!!giveLinkURL}
                handleClose={handleGiveLinkClose}
            />
        </div>
    );
};

export default Home;
