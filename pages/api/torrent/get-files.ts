import { NextApiRequest, NextApiResponse } from "next";
import torrentStream from "torrent-stream";
import { ErrorResult, TorrentFiles } from "lib/data";

const handler = async (
    req: NextApiRequest,
    res: NextApiResponse<TorrentFiles | ErrorResult>
) => {
    try {
        const { torrentURI } = req.body;
        const engine = torrentStream(torrentURI, {
            uploads: 0,
            tmp: process.env.TSTREAM_TMP,
        });

        const results: TorrentFiles = {};
        await new Promise((r) => engine.on("ready", r));
        engine.files.forEach((file) => {
            results[file.path] = {
                name: file.name,
                path: file.path,
                length: file.length,
            };
        });
        res.status(200).json(results);
    } catch (err: any) {
        res.status(500).json({
            error: err.message,
        });
    }
};

export default handler;
