import { v4 as uuidv4 } from "uuid";

export interface TorrentStreamURL {
    id: string;
    torrentURI: string;
    filePath: string;
}

export interface TorrentStreamURLs {
    [key: string]: TorrentStreamURL;
}

export const createNewTorrentStreamURL = (
    torrentURI: string,
    filePath: string
): TorrentStreamURL => {
    return {
        id: uuidv4(),
        torrentURI,
        filePath,
    };
};

export const createNewTorrentStreamURLAndAddToPool = (
    torrentURI: string,
    filePath: string
) => {
    const result = createNewTorrentStreamURL(torrentURI, filePath);
    torrentStreamUrls[result.id] = result;
    return result;
};

const torrentStreamUrls: TorrentStreamURLs = {};
(global as any).torrentStreamUrls = torrentStreamUrls;
